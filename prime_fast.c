//山本浩士　素数解析コンテスト用

//課題:『put out sum of primes in [a:b]』
//評価基準:１分以内に処理できる任意のx~x+1000間のxの最大値で評価
//結果:学生１１名中１位 x=21京　ただし、先生に勝てなかったため２日後に改善してリベンジ勝利。（このソースは、改善後コメントを加えたもの）
//用いた高速化の方法:2と3以外の素数は6の自然数倍の±1に入ることと、約数の性質から素数判定対象数の平方根までに約数が存在しなければ判定対象は素数ということを用いた。
//どの範囲の数字が与えられても正常に動作させるために:要所要所にprint出力と、例外処理を加えてある。

#include <stdio.h>
#include <math.h>
int main(void)
{
  int yakusu,i,r,t;
  unsigned long long int sum=0,a,b,box;

  printf("範囲内の素数の和を求める。\n 範囲を入力してください。[a:b]\n");
  printf("a=");
  scanf("%llu",&a);
  printf("b=");
  scanf("%llu",&b);
  printf("範囲は%llu~%llu \n",a,b);

  if(b<=1 || a<0){printf("範囲外、やり直し。"); return 0;}
  printf("範囲は%llu~%llu \n",a,b);
  printf("範囲中の素数は");
  if(a%6==0 && a!=0){goto SIX;}

  if(a<=2 && 2<=b){
    sum+=2;

    a=3;
    printf("2,");
  }

  if(a%2==0){a=a+1;}//判定対象が偶数かつ２以上の時、aに１を足し奇数にする。

  for(a;a<=b;a+=2)
    {
      yakusu=0;
      r=sqrt(a);
      if(a%3==0 && a!=3){yakusu+=1;}
      //iは6n-1,tは6n+1を担当している。     
      for(i=5;i<=r;i+=6)
	{

	  t=i+2;


	  if(a%i==0 || a%t==0)
 
	    {
	      yakusu+=1;
	      break;
	    }
	}
      if(yakusu==0) {
	printf("%llu,",a);
	sum+=a;}

    }

  printf("和は%lluです。\n",sum); return 0;



 SIX:   
  a=a+1;   //範囲中の6n+1を探索
  box=a;
  for(a;a<=b;a+=6)
    {
      yakusu=0;
      r=sqrt(a);
      for(i=5;i<=r;i+=6)
	{ 
	  t=i+2;
	  if(a%i==0 ||a%t==0)
	    {

	      yakusu+=1;
	      break;
	    }
	}

      if(yakusu==0) {
	printf("%llu,",a);
	sum+=a;}
    }

  a=box+4;//6n+1から6(n+1)-1までの距離は４
  for(a;a<=b;a+=6)
    {

      yakusu=0;
      r=sqrt(a);
      for(i=5;i<=r;i+=6)
	{

	  t=i+2;
	  if(a%i==0 || a%t==0)
	    {
	      yakusu+=1;
	      break;
	    }
	}

      if(yakusu==0) {
	printf("%llu,",a);
	sum+=a;}
    }

  printf("\n 素数の和は%lluです。\n",sum);

  return 0;
}
